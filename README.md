# The PTT Oil Price getter
This is app to get oil price from PTT thailand split in province & distinct

# Installation
    - Requirement python version 3.5+
    - If you not install package you can install first in 'requirements.txt'

```bash
pip install -r requirements.txt
```

# Starting
```bash
python3 api.py --year 2022
```