#!/usr/local/bin/python3
# -*- coding: utf-8 -*-
import sys
import json
import requests
import argparse
import csv
from datetime import datetime
from typing import Dict, List, Tuple

class PttOilPrice:
    def __init__(self):
        self.URL = "https://orapiweb2.pttor.com/api/oilprice/search"
        self.client = requests
        self.type_oil = {
            1 : "Diesel Premium B7",
            2 : "Diesel",
            5 : "Diesel B20",
            7 : "Bensine",
            9 : "Gasohol 95",
            10 : "Gasohol 91",
            11 : "E20",
            17 : "Diesel B7",
        }
        self.header = ["date","time","province","district","diesel_premium_b7","diesel","diesel_b20","benzine","gasohol_95","gasohol_91","e20","diesel_b7"]
        self.data = {}
        self.OK = 200
        self.provinces = {}
        self.distincts = {}
        self.keys = {}

        self.from_year = 0
        self.from_month = 0
        self.from_date = 0
        self.from_timestamp = 0.0

        self.to_year = 0
        self.to_month = 0
        self.to_date = 0
        self.to_timestamp = 0.0
        self.load_json()
    
    """
        Function : type_oil
        @sync
        About : Convert type oil id to oil string
        @param : 
            - Integer : OilTypeId
        @Return {String}
    """
    def type_oil(self,OilTypeId) -> str:
        try:
            return self.type_oil[OilTypeId]
        except ValueError:
            return ""
    
    def load_json(self):
        with open("key.json") as f:
            data = json.loads(f.read())
        self.keys = data

    """
        Function : packet
        @sync
        About : Map data to array packet
        @Param :
            - Json : data
            - Float : diesel_premium_b7
            - Float : diesel,
            - Float : diesel_b20
            - Float : benzine
            - Float : gasohol_95
            - Float : gasohol_91
            - Float : e20
            - Float : diesel_b7
        @Return {Array}
    """
    def packet(self,data,diesel_premium_b7,diesel,diesel_b20,benzine,gasohol_95,gasohol_91,e20,diesel_b7) -> List:
        date = "{}-{}-{}".format(data['year'],data['month'],data['day'])

        """
        datetime_object = datetime.strptime(date,"%Y-%m-%d")
        datetime_stamp = datetime.timestamp(datetime_object)
        print(datetime_stamp)
        if datetime_stamp not in range(int(self.from_timestamp),int(self.to_timestamp)+1):
            return []
        """
        return [
            date,
            "05:00:00",
            data['province']['titleTH'],
            data['district']['titleTH'],
            diesel_premium_b7,
            diesel,
            diesel_b20,
            benzine,
            gasohol_95,
            gasohol_91,
            e20,
            diesel_b7
        ]

    """
        Function : initial
        @sync
        About : Initial json data before call ptt api
        @Param :
            - Integer : provinceId
            - Integer : year
            - Integer : month
            - Integer : pageSize
            - Integer : pageIndex
    """  
    def initial(self,provinceId,districtId,year,month,pageSize=1000000,pageIndex=0):
        self.data = {
            "provinceId":provinceId,
            "year":year,
            "month":month,
            "pageSize":pageSize,
            "pageIndex":pageIndex
        }

        if districtId > 0:
            self.data['districtId'] = districtId
    
    """
        Function : load
        @sync
        About : Load datas and map to array
        @Param :
            - Json : datas
        @Return : {Array}
    """
    def load(self,datas = {}) -> List:
        arrs = []
        for data in datas:
            diesel_premium_b7 = 0
            diesel = 0
            diesel_b20 = 0
            benzine = 0
            gasohol_95 = 0
            gasohol_91 = 0
            e20 = 0
            diesel_b7 = 0
            for price in json.loads(data['priceData']):
                if price['OilTypeId'] == 1:
                    diesel_premium_b7 = price['Price']
                elif price['OilTypeId'] == 2:
                    diesel = price['Price']
                elif price['OilTypeId'] == 5:
                    diesel_b20 = price['Price']
                elif price['OilTypeId'] == 7:
                    benzine = price['Price']
                elif price['OilTypeId'] == 9:
                    gasohol_95 = price['Price']
                elif price['OilTypeId'] == 10:
                    gasohol_91 = price['Price']
                elif price['OilTypeId'] == 11:
                    e20 = price['Price']
                elif price['OilTypeId'] == 17:
                    diesel_b7 = price['Price']
            
            arr = self.packet(
                    data,
                    diesel_premium_b7,diesel,diesel_b20,
                    benzine,gasohol_95,gasohol_91,e20,diesel_b7
                )
            if arr:
                arrs.append(arr)
        return arrs

    """
        Function : export_csv
        @sync
        About : Create data in CSV File
    """
    def export_csv(self,datas,from_date,to_date,province,district=""):
        path  = "Export_PTT_Oil_Price_{}_{}_From_{}_To_{}_{}.csv".format(province,district,from_date,to_date,datetime.now())
        with open(path, 'w', encoding='UTF8', newline='') as f:
            writer = csv.writer(f)
            # write the header
            writer.writerow(self.header)
            # write multiple rows
            writer.writerows(datas)
    
    """
        Function : post
        @sync
        About : Call PTT api in method POST
        @Return {Json}
    """
    def post(self) -> Dict:
        resp = self.client.post(self.URL,json=self.data)

        if not resp.status_code == self.OK:
            return {}
        
        json_data = resp.json()
        if not json_data['success']:
            return {}
        
        if not json_data['data']:
            return {}
        
        return json_data['data']
    
    def get_key(self,province,district) -> Tuple:
        
        if province and district:
            key = "{}-{}".format(province,district)
            if key in self.keys:
                new_key = self.keys[key]
                return (new_key['province_id'],new_key['district_id'])
        
        if province and not district:
            for prov in self.keys:
                if province in prov:
                    return (self.keys[prov]['province_id'],0)
        
        return ()
    
    def initial_date(self,date_str) -> List:
        if not date_str:
            return []
        
        split_date = date_str.split("-")

        if len(split_date) != 3:
            return []

        if len(split_date[0]) != 4:
            return []
        
        
        return [int(split_date[0]),int(split_date[1]),int(split_date[2])]

    """
        Function : process
        @sync
        About : The main process input argument year
        @Param : 
            - Integer : year
    """
    def process(self,
    province = "",
    district = "",
    from_date="{}-1-1".format(datetime.now().year),
    to_date=datetime.now().strftime("%Y-%m-%d")
    ):
        keys = self.get_key(province,district)
        arrs = []
        new_arrs = []
        if not keys:
            print("Cannot found province or district")
            sys.exit()
        
        province_id = keys[0]
        district_id = keys[1]

        from_datetime = datetime.strptime(from_date,"%Y-%m-%d")
        self.from_timestamp = datetime.timestamp(from_datetime)

        to_datetime = datetime.strptime(to_date,"%Y-%m-%d")
        self.to_timestamp = datetime.timestamp(to_datetime)

        [self.from_year,self.from_month,self.from_date] = self.initial_date(from_date)
        [self.to_year,self.to_month,self.to_date] = self.initial_date(to_date)

        for idx in range(self.from_year,self.to_year+1):
            year = self.from_year+(idx-self.from_year)
            for month in range(self.from_month,13):
                print("{}-{} : processing..".format(year,month))
                self.initial(province_id,district_id,year,month)
                datas = self.post()
                arrs.extend(self.load(datas))
                if month == self.to_month and year == self.to_year:
                    break
        
        for idx,arr in enumerate(arrs):
            datetime_object = datetime.strptime(arr[0],"%Y-%m-%d")
            datetime_stamp = datetime.timestamp(datetime_object)
            if int(datetime_stamp) >= int(self.from_timestamp)  and int(datetime_stamp) <= int(self.to_timestamp):
                new_arrs.append(arr)
        
        self.export_csv(arrs,from_date,to_date,province,district)
        print("Processing successfully!.")

if __name__ == "__main__":
    try:
        print("Starting program...")
        parser = argparse.ArgumentParser(description='Process get PTT Oil prices')
        parser.add_argument(
                                '--year', 
                                type=int,
                                help="Get year for query price Ex. 2022",
                                default=datetime.now().year, 
                            )
        parser.add_argument(
                                '--month', 
                                type=int,
                                help="Get month for query price Ex. 1 = january , 12 = december",
                                default=datetime.now().month, 
                            )
        
        parser.add_argument(
                                '--province', 
                                type=str,
                                help="Get province for query price",
                                default="อุบลราชธานี",
                            )
        
        parser.add_argument(
                                '--district', 
                                type=str,
                                help="Get district for query price",

                            )

        parser.add_argument(
                                '--from_date', 
                                type=str,
                                help="Get date to start query [2022-1-1]",
                                default="{}-1-1".format(datetime.now().year)
                            )
        
        parser.add_argument(
                                '--to_date', 
                                type=str,
                                help="Get date to stop query [2022-12-31]",
                                default=datetime.now().strftime("%Y-%m-%d")
                            )
        args = parser.parse_args()
        ptt = PttOilPrice()
        data = ptt.process(args.province,args.district,args.from_date,args.to_date)
    except KeyboardInterrupt:
        print("Exit program..")
        sys.exit()

